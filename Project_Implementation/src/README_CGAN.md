# An Image Captioning codebase using Conditional GAN

This is a codebase for image captioning research.

It is referenced from the research paper
Improving Image Captioning with Conditional Generative Adversarial Nets - https://arxiv.org/pdf/1805.07112.pdf

## Dataset
Coco dataset was used with Zebra class only

Download the processed data used for experiments directly from https://drive.google.com/drive/folders/1Qy2UjXk_DKcRc7kXVVnykOFv9SFPPJXF?usp=sharing

OR

The following steps are followed to preprocess the data
- Place the data (train2014 and val2014) folders in the data/image_data folder
- Download dataset_coco.json (http://cs.stanford.edu/people/karpathy/deepimagesent/caption_datasets.zip) and place it in data folder
- Change the directory location in prepro_labels.py on line 171 and 177, prepro_feats.py line 67 and prepro_ngrams.py and specify the arguments
- Run prepro_labels.py, prepro_feats.py and prepro_ngrams.py respectively to generate the preprocessed data files to be used in training

## Training
Training the GAN model is split into the following three steps:
- MLE training of the generator: Run train_mle.py to pretrain the generator using MLE.
- Pretraining the discriminator: Run pretrain_discriminator.py to pretrain the discriminator.
- Adverserial training: Run train_pg.py to train the generator and discriminator. Do not forget to specify the pretrained models file name in the arguments for
both generator and discriminator

## Visualizing using Tensorboard
The training loss can be visualized using tensorboard using the following command 
tensorboard --logdir tb_ckpts/<dir_name>
The <dir_name> for the 3 training phases are as follows:
- Generator MLE training: gen_mle
- Pretrain Discriminator: dis
- Adversarial training: pg

## Evaluation
The model performance can be calculated using the evalModel.py script. It calculates the CIDEr and BLEU scores for 10 random batches from the testing dataset
including the average of each score. Follow these steps to run the script
- Place the trained generator model in ckpts/coco/gen and rename the model file name in arguments for evalModel.py
- Place the trained discriminator model in ckpts/coco/dis and rename the model file name in arguments for evalModel.py

The trained models can be downloaded from https://drive.google.com/drive/folders/1XSZRT_THrEBMZgpTRxF1HuNH_uitdgYP?usp=sharing

## References:
- Improving Image Captioning with Conditional Generative Adversarial Nets - https://arxiv.org/pdf/1805.07112.pdf
- Self critical sequence training repository - https://github.com/ruotianluo/self-critical.pytorch
- Image captioning using SeqGAN - https://github.com/Anjaney1999/image-captioning-seqgan


