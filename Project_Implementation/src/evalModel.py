import argparse
import logging
import os.path as path
from collections import OrderedDict

from nltk.translate.bleu_score import corpus_bleu
from torch import optim, nn

from captioning.utils import opts
from data.dataloader import DataLoader
from modelsFile import *
from train_pg import sample_from_start
from utils import *
from captioning.utils.rewards import init_scorer, array_to_str, get_scores


def main(args):
    opt = opts.parse_opt()

    # Setting test configs in opt
    opt.input_fc_dir = 'data/val/cocotalk_fc'
    opt.input_att_dir = 'data/val/cocotalk_att'
    opt.input_label_h5 = 'data/cocotalk_test_label.h5'
    opt.cocotalk_file = 'data/cocotalk_test.json'
    # opt.batch_size = 677 # Size of the entire dataset
    opt.val_only = True

    loader = DataLoader(opt)
    opt.vocab_size = loader.vocab_size + 1
    opt.seq_length = loader.seq_length
    opt.vocab = loader.get_vocab()

    # initializing scorer object for SCST training
    init_scorer('coco-all-test-idxs')

    gen_checkpoint_path = args.storage + '/ckpts/' + args.dataset + '/gen/' + args.gen_checkpoint_filename
    dis_checkpoint_path = args.storage + '/ckpts/' + args.dataset + '/dis/' + args.dis_checkpoint_filename

    generator = Generator(attention_dim=args.attention_dim, gru_units=args.gen_gru_units, vocab_size=opt.vocab_size,
                          embedding_dim=args.gen_embedding_dim)
    generator.to(device)
    discriminator = GRUDiscriminator(embedding_dim=args.dis_embedding_dim, gru_units=args.dis_gru_units,
                                     vocab_size=opt.vocab_size, encoder_dim=2048)
    discriminator.to(device)

    word_index = {'<start>': -1, '<pad>': 0, '<end>': -1}  # Processed data does not have start and end tags

    gen_optimizer = optim.Adam(generator.parameters(), lr=args.gen_lr)
    dis_optimizer = optim.Adam(discriminator.parameters(), lr=args.dis_lr)
    dis_criterion = nn.BCELoss().to(device)
    gen_pg_criterion = nn.CrossEntropyLoss(reduction='none', ignore_index=word_index['<pad>']).to(device)
    gen_mle_criterion = nn.CrossEntropyLoss(reduction='sum', ignore_index=word_index['<pad>']).to(device)

    if path.isfile(gen_checkpoint_path):
        logging.info('loaded generator checkpoint')
        checkpoint = torch.load(gen_checkpoint_path)
        generator.load_state_dict(checkpoint['gen_state_dict'])
        generator.to(device)
        if args.gen_checkpoint_filename.split('_')[0] == 'PG':
            gen_optimizer.load_state_dict(checkpoint['optimizer_state_dict'])
            gen_batch_id = checkpoint['gen_batch_id']
            gen_epoch = checkpoint['gen_epoch']

    if path.isfile(dis_checkpoint_path):
        logging.info('loaded discriminator checkpoint')
        checkpoint = torch.load(dis_checkpoint_path)
        discriminator.load_state_dict(checkpoint['dis_state_dict'])
        if args.dis_checkpoint_filename.split('_')[0] == 'PG':
            dis_optimizer.load_state_dict(checkpoint['optimizer_state_dict'])
            dis_batch_id = checkpoint['dis_batch_id']
            dis_epoch = checkpoint['dis_epoch']
    cs_scores=[]
    bl_scores=[]
    for run in range(10):
        dataset = loader.dataset
        label_lengths = dataset.h5_label_file["label_length"]
        data = loader.get_batch('val')
        tmp = [data['fc_feats'], data['att_feats'], data['labels'], data['masks'], data['att_masks']]
        # tmp = [_ if _ is None else _.cuda() for _ in tmp]
        fc_feats, att_feats, labels, masks, att_masks = tmp
        batch_size = fc_feats.shape[0]
        seq_per_img = labels.shape[0] // batch_size
        cap_lens = label_lengths[
                   data["bounds"]["it_pos_now"]: data["bounds"][
                                                     "it_pos_now"] + batch_size * seq_per_img:seq_per_img]
        cap_lens = torch.Tensor(cap_lens.tolist()).to(torch.int64)

        imgs, caps = fc_feats.to(device), labels.to(device).to(torch.int64)

        matching_caps = caps.clone()

        # select only 1 out of 5 captions for each image
        caps = caps[:, 0, :]
        caps = caps[:, 1:]  # Removing the extra zero at the beginning

        mismatched_index = np.random.randint(len(cap_lens), size=len(cap_lens))
        mismatched_imgs = fc_feats[mismatched_index]

        gts = data['gts']
        gt_indices = torch.arange(0, len(gts))
        data_gts = [gts[_] for _ in gt_indices.tolist()]
        fake_caps, fake_cap_lens, hidden_states = sample_from_start(imgs, caps, cap_lens, generator, word_index, args)
        pg_preds, pg_caps, pg_output_lens, pg_indices = generator(imgs, caps, cap_lens)

        opt.cider_reward_weight = 1
        opt.bleu_reward_weight = 1
        get_scores(data_gts, fake_caps, opt)
        print("Using gen")
        word_idxs = torch.max(pg_preds, dim=2)[1]
        cs = get_scores(data_gts, word_idxs, opt)

        hypotheses_tf=[]
        references=[]
        matching_caps = matching_caps[pg_indices]
        for cap_set in matching_caps.tolist():
            refs = []
            for caption in cap_set:
                cap = [word_id for word_id in caption
                       if word_id != word_index['<start>'] and word_id != word_index['<pad>']]
                refs.append(cap)
            references.append(refs)
        word_idxs, _ = pad_generated_captions(word_idxs.cpu().numpy(), word_index)
        for idxs in word_idxs.tolist():
            hypotheses_tf.append(
                [idx for idx in idxs if idx != word_index['<start>'] and idx != word_index['<pad>']])

        bleu_1_tf = corpus_bleu(references, hypotheses_tf, weights=(1, 0, 0, 0))
        print("Corpus Bleu score ", bleu_1_tf)
        cs_scores.append(cs)
        bl_scores.append(bleu_1_tf)

    print("CIDEr scores ", cs_scores)
    print("BLEU scores ", bl_scores)
    print("Average CIDEr score ", np.average(cs_scores))
    print("Average BLEU score ", np.average(bl_scores))


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Adversarial Training via Policy Gradients')
    parser.add_argument('--storage', type=str, default='.')
    parser.add_argument('--image-path', type=str, default='images')
    parser.add_argument('--dataset', type=str, default='coco')
    parser.add_argument('--max-len', type=int, default=17)
    parser.add_argument('--gen-checkpoint-filename', type=str, default='TRAIN_PG_GEN_G-STEPS_1_D-STEPS_1_CNN-ARCH_resnet152_1400_0_1401.pth')
    parser.add_argument('--dis-checkpoint-filename', type=str, default='pretrain_dis_9_multinomial_resnet152.pth')
    parser.add_argument('--sampling-method', type=str, default='multinomial')
    parser.add_argument('--dis-embedding-dim', type=int, default=512)
    parser.add_argument('--dis-gru-units', type=int, default=512)
    parser.add_argument('--gen-embedding-dim', type=int, default=512)
    parser.add_argument('--gen-gru-units', type=int, default=512)
    parser.add_argument('--attention-dim', type=int, default=512)
    parser.add_argument('--gen-lr', type=float, default=1e-4)
    parser.add_argument('--dis-lr', type=float, default=1e-4)

    main(parser.parse_args())

