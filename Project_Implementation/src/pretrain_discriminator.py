import argparse
import json
import torch
import torch.nn as nn
import torch.optim as optim
from tensorboardX import SummaryWriter
from torchvision import transforms
from torch.nn.utils.rnn import pack_padded_sequence
import os.path as path
import logging
import time
import csv

from captioning.modelsFile import *
from utils import *
from captioning.utils import opts
from data.dataloader import DataLoader

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

data_transforms = transforms.Compose([
    transforms.Resize((224, 224)),
    transforms.ToTensor(),
    transforms.Normalize(mean=[0.485, 0.456, 0.406],
                         std=[0.229, 0.224, 0.225])
])

logging.basicConfig(level=logging.INFO)


def main(args):
    opt = opts.parse_opt()
    loader = DataLoader(opt)
    opt.vocab_size = loader.vocab_size + 1
    opt.seq_length = loader.seq_length

    encoder = None

    generator = Generator(embedding_dim=args.gen_embedding_dim,
                          attention_dim=args.attention_dim,
                          gru_units=args.gen_gru_units,
                          vocab_size=opt.vocab_size)
    generator.to(device)

    discriminator = GRUDiscriminator(embedding_dim=args.dis_embedding_dim,
                                     gru_units=args.dis_gru_units,
                                     vocab_size=opt.vocab_size,
                                     encoder_dim=2048)

    discriminator.to(device)

    word_index = {'<start>': -1, '<pad>': 0, '<end>': -1}  # Processed data does not have start and end tags

    # tensorboard logger
    tb_summary_writer = SummaryWriter(opt.checkpoint_path + '\\dis')

    dis_optimizer = optim.Adam(discriminator.parameters(), lr=args.lr)
    scheduler = torch.optim.lr_scheduler.StepLR(dis_optimizer, step_size=args.step_size, gamma=0.5)
    dis_criterion = nn.BCELoss().to(device)

    gen_checkpoint_path = args.storage + '/ckpts/' + args.dataset + '/gen/' + args.gen_checkpoint_filename
    dis_checkpoint_path = args.storage + '/ckpts/' + args.dataset + '/dis/' + args.dis_checkpoint_filename

    if path.isfile(gen_checkpoint_path):
        checkpoint = torch.load(gen_checkpoint_path)
        generator.load_state_dict(checkpoint['gen_state_dict'])
        logging.info('loaded generator')
        generator.to(device)

    encoder = Encoder(args.cnn_architecture)
    encoder.to(device)

    if path.isfile(dis_checkpoint_path):
        checkpoint = torch.load(dis_checkpoint_path)
        discriminator.load_state_dict(checkpoint['dis_state_dict'])
        dis_optimizer.load_state_dict(checkpoint['dis_optimizer_state_dict'])
        logging.info('loaded discriminator')
        discriminator.to(device)

    for e in range(args.epochs):
        train(epoch=e, generator=generator, encoder=encoder,
              discriminator=discriminator, dis_optimizer=dis_optimizer,
              dis_criterion=dis_criterion, train_loader=loader,
              word_index=word_index, args=args, tb_summary_writer=tb_summary_writer)

        if args.save_model:
            torch.save(
                {'dis_state_dict': discriminator.state_dict(), 'optimizer_state_dict': dis_optimizer.state_dict()},
                args.storage + '/ckpts/' + args.dataset +
                '/dis/{}_{}_{}_{}.pth'.format('PRETRAIN_DIS', e,
                                                 args.sampling_method,
                                                 args.cnn_architecture))
        print('Completed epoch: ' + str(e))

        scheduler.step()


def sample_from_start(imgs, caps, cap_lens, generator, word_index, args):
    with torch.no_grad():
        fake_caps, hidden_states = generator.sample(cap_len=max(torch.max(cap_lens).item(), args.max_len) - 1,
                                                    col_shape=caps.shape[1],
                                                    img_feats=imgs,
                                                    input_word=caps[:, 0],
                                                    hidden_state=None, sampling_method=args.sampling_method)

        fake_caps, fake_cap_lens = pad_generated_captions(fake_caps.cpu().numpy(), word_index)
        fake_caps, fake_cap_lens = torch.LongTensor(fake_caps).to(device), torch.LongTensor(fake_cap_lens)

        return fake_caps, fake_cap_lens, hidden_states


def train(epoch, encoder, generator, discriminator, dis_optimizer, dis_criterion, train_loader, word_index, args, tb_summary_writer):
    losses = AverageMeter()
    acc = AverageMeter()
    if not args.use_image_features:
        encoder.eval()

    discriminator.train()
    generator.eval()

    # for batch_id, (imgs, mismatched_imgs, caps, cap_lens) in enumerate(train_loader):

    dataset = train_loader.dataset
    label_lengths = dataset.h5_label_file["label_length"]
    data = train_loader.get_batch('train')
    tmp = [data['fc_feats'], data['att_feats'], data['labels'], data['masks'], data['att_masks']]
    # tmp = [_ if _ is None else _.cuda() for _ in tmp]
    fc_feats, att_feats, labels, masks, att_masks = tmp
    batch_size = fc_feats.shape[0]
    seq_per_img = labels.shape[0] // batch_size
    cap_lens = label_lengths[
               data["bounds"]["it_pos_now"]: data["bounds"]["it_pos_now"] + batch_size * seq_per_img:seq_per_img]
    cap_lens = torch.Tensor(cap_lens.tolist()).to(torch.int64)

    mismatched_index = np.random.randint(len(cap_lens), size=len(cap_lens))
    mismatched_imgs = fc_feats[mismatched_index]

    start_time = time.time()

    imgs, mismatched_imgs, caps = fc_feats.to(device), mismatched_imgs.to(device), labels.to(device).to(torch.int64)
    # cap_lens = cap_lens.squeeze(-1)

    # select only 1 out of 5 captions for each image
    caps = caps[:, 0, :]

    if not args.use_image_features:
        imgs = encoder(imgs)
        mismatched_imgs = encoder(mismatched_imgs)

    fake_caps, fake_cap_lens, _ = sample_from_start(imgs, caps, cap_lens, generator, word_index, args)
    ones = torch.ones(caps.shape[0]).to(device)
    zeros = torch.zeros(caps.shape[0]).to(device)

    dis_optimizer.zero_grad()
    true_preds = discriminator(imgs, caps, cap_lens)
    false_preds = discriminator(mismatched_imgs, caps, cap_lens)
    fake_preds = discriminator(imgs, fake_caps, fake_cap_lens)
    # loss = dis_criterion(true_preds, ones) + 0.5 * dis_criterion(false_preds, zeros) + \
    #        0.5 * dis_criterion(fake_preds, zeros)
    loss = applyLogToTensor(dis_criterion(true_preds, ones)) + 0.5 * applyLogToTensor(1 - dis_criterion(false_preds, zeros)) + \
           0.5 * applyLogToTensor(1 - dis_criterion(fake_preds, zeros))
    loss.backward()
    dis_optimizer.step()
    losses.update(loss.item())
    true_acc = binary_accuracy(true_preds, ones).item()
    false_acc = binary_accuracy(false_preds, zeros).item()
    fake_acc = binary_accuracy(fake_preds, zeros).item()
    avg_acc = (true_acc + false_acc + fake_acc) / 3.0
    acc.update(avg_acc)

        # if batch_id % args.print_freq == 0:
    tb_summary_writer.add_scalar('dis_train_loss', loss, epoch)
    print('Epoch: [{}]\t'
                 'Time per batch: [{:.3f}]\t'
                 'Loss [{:.4f}]({:.3f})\t'
                 'Accuracy [{:.4f}]({:.3f})'.format(epoch, time.time() - start_time, losses.avg,
                                                    losses.val, acc.avg, acc.val))

    if args.save_stats:
        with open(args.storage + '/stats/' + args.dataset +
                  '/dis/{}_{}.csv'.format('PRETRAIN_DIS', args.cnn_architecture), 'a+') as file:
            writer = csv.writer(file)
            writer.writerow(
                [epoch, losses.avg, losses.val, acc.val, acc.avg])


def applyLogToTensor(tens):
    idx = tens != 0
    tens[idx] = torch.log(tens[idx])
    return tens


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Pre-train discriminator')
    parser.add_argument('--batch-size', type=int, default=32)
    parser.add_argument('--epochs', type=int, default=100)
    parser.add_argument('--lr', type=float, default=5e-4)
    parser.add_argument('--step-size', type=float, default=5)
    parser.add_argument('--print-freq', type=int, default=50)
    parser.add_argument('--sampling-method', type=str, default='multinomial')
    parser.add_argument('--cnn-architecture', type=str, default='resnet152')
    parser.add_argument('--max-len', type=int, default=18)
    parser.add_argument('--storage', type=str, default='.')
    parser.add_argument('--image-path', type=str, default='images')
    parser.add_argument('--dataset', type=str, default='coco')
    parser.add_argument('--dis-embedding-dim', type=int, default=512)
    parser.add_argument('--dis-gru-units', type=int, default=512)
    parser.add_argument('--gen-embedding-dim', type=int, default=512)
    parser.add_argument('--gen-gru-units', type=int, default=512)
    parser.add_argument('--attention-dim', type=int, default=512)
    parser.add_argument('--gen-checkpoint-filename', type=str, default='mle_gen_resnet152_5.pth')
    parser.add_argument('--dis-checkpoint-filename', type=str, default='')
    parser.add_argument('--use-image-features', type=bool, default=True)
    parser.add_argument('--save-model', type=bool, default=True)
    parser.add_argument('--save-stats', type=bool, default=False)
    parser.add_argument('--workers', type=int, default=2)
    main(parser.parse_args())
