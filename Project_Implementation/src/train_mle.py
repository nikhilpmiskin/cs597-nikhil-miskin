import argparse
import csv
import logging
import time

import torch
from nltk.translate.bleu_score import corpus_bleu
from tensorboardX import SummaryWriter
from torch import optim
from torch.nn.utils.rnn import pack_padded_sequence

from data.dataloader import DataLoader
from captioning.modelsFile import Generator, device, Encoder
from utils import AverageMeter, categorical_accuracy, pad_generated_captions
from captioning.utils import opts


def main(args):
    opt = opts.parse_opt()
    loader = DataLoader(opt)
    opt.vocab_size = loader.vocab_size+1
    opt.seq_length = loader.seq_length

    generator = Generator(attention_dim=args.attention_dim,
                          gru_units=args.gru_units,
                          vocab_size=opt.vocab_size,
                          embedding_dim=args.embedding_dim)

    generator.to(device)
    optimizer = optim.Adam(generator.parameters(), lr=args.lr)
    scheduler = torch.optim.lr_scheduler.StepLR(optimizer, step_size=args.step_size, gamma=0.5)
    criterion = torch.nn.CrossEntropyLoss(reduction='sum').to(device)

    encoder = Encoder(args.cnn_architecture)
    encoder.to(device)

    word_index = {'<start>': -1, '<pad>': 0, '<end>': -1}

    # tensorboard logger
    tb_summary_writer = SummaryWriter(opt.checkpoint_path + '\\gen_mle')

    for e in range(args.epochs):
        gen_mle_train(epoch=e, encoder=encoder, generator=generator,
                      optimizer=optimizer, criterion=criterion,
                      train_loader=loader, args=args, tb_summary_writer=tb_summary_writer)

        validate(epoch=e, encoder=encoder, generator=generator,
                 criterion=criterion, val_loader=loader,
                 word_index=word_index, args=args)

        if args.save_model:
            torch.save({
                'gen_state_dict': generator.state_dict(),
                'optimizer_state_dict': optimizer.state_dict(),
                'scheduler_state_dict': scheduler.state_dict()
            }, args.storage + '/ckpts/' + args.dataset + '/gen/{}_{}_{}.pth'.format('MLE_GEN',
                                                                                    args.cnn_architecture, e))
        scheduler.step()


def gen_mle_train(epoch, encoder, generator, optimizer, criterion, train_loader, args, tb_summary_writer):
    losses = AverageMeter()
    top5 = AverageMeter()
    top1 = AverageMeter()

    if not args.use_image_features:
        encoder.eval()

    generator.train()

    # for batch_id, (imgs, caps, cap_lens) in enumerate(train_loader):
    dataset = train_loader.dataset
    label_lengths = dataset.h5_label_file["label_length"]
    data = train_loader.get_batch('train')
    tmp = [data['fc_feats'], data['att_feats'], data['labels'], data['masks'], data['att_masks']]
    # tmp = [_ if _ is None else _.cuda() for _ in tmp]
    fc_feats, att_feats, labels, masks, att_masks = tmp
    batch_size = fc_feats.shape[0]
    seq_per_img = labels.shape[0] // batch_size
    cap_lens = label_lengths[data["bounds"]["it_pos_now"]: data["bounds"]["it_pos_now"]+batch_size*seq_per_img:seq_per_img]
    cap_lens = torch.Tensor(cap_lens.tolist()).to(torch.int64)
    start_time = time.time()

    imgs, caps = fc_feats.to(device), labels.to(device).to(torch.int64)

    # select only 1 out of 5 captions for each image
    caps = caps[:,0,:]

    # cap_lens = cap_lens.squeeze(-1)

    if not args.use_image_features:
        imgs = encoder(imgs)

    optimizer.zero_grad()
    preds, caps, output_lens, indices = generator(imgs, caps, cap_lens)
    loss = 0.0
    for i in range(caps.shape[0]):
        loss += criterion(preds[i, :], caps[i, 1:])
    loss = loss / (1.0 * caps.shape[0])
    # loss += args.alpha_c * ((1. - alphas.sum(dim=1)) ** 2).mean()
    preds = pack_padded_sequence(preds, output_lens, batch_first=True)[0]
    targets = pack_padded_sequence(caps[:, 1:], output_lens, batch_first=True)[0]
    loss.backward()
    torch.nn.utils.clip_grad_norm_(generator.parameters(), args.clip)
    optimizer.step()
    top1_acc = categorical_accuracy(preds, targets, 1)
    top1.update(top1_acc, sum(output_lens))
    top5_acc = categorical_accuracy(preds, targets, 5)
    top5.update(top5_acc, sum(output_lens))
    losses.update(loss.item(), sum(output_lens))

    # if batch_id % args.print_freq == 0:
    tb_summary_writer.add_scalar('mle_train_loss', loss, epoch)
    print('Epoch: [{}]\t'
                 'Time per batch: [{:.3f}]\t'
                 'Loss [{:.4f}]({:.3f})\t'
                 'Top 5 accuracy [{:.4f}]({:.3f})\t'
                 'Top 1 accuracy [{:.4f}]({:.3f})\t'.format(epoch, time.time() - start_time,
                                                            losses.avg, losses.val, top5.avg, top5.val,
                                                            top1.avg, top1.val))

    if args.save_stats:
        with open(args.storage + '/stats/' + args.dataset + '/gen/TRAIN_MLE_GEN.csv', 'a+') as file:
            writer = csv.writer(file)
            writer.writerow([epoch, losses.avg, losses.val, top5.avg, top5.val, top1.avg, top1.val])


def validate(epoch, encoder, generator, criterion, val_loader, word_index, args):
    losses = AverageMeter()
    top5 = AverageMeter()
    top1 = AverageMeter()

    if not args.use_image_features:
        encoder.eval()

    generator.eval()

    references = []
    hypotheses = []
    hypotheses_tf = []

    with torch.no_grad():

        # for batch_id, (imgs, caps, cap_lens, matching_caps) in enumerate(val_loader):
        dataset = val_loader.dataset
        label_lengths = dataset.h5_label_file["label_length"]
        data = val_loader.get_batch('train')
        tmp = [data['fc_feats'], data['att_feats'], data['labels'], data['masks'], data['att_masks']]
        # tmp = [_ if _ is None else _.cuda() for _ in tmp]
        fc_feats, att_feats, labels, masks, att_masks = tmp
        batch_size = fc_feats.shape[0]
        seq_per_img = labels.shape[0] // batch_size
        cap_lens = label_lengths[
                   data["bounds"]["it_pos_now"]: data["bounds"]["it_pos_now"] + batch_size * seq_per_img:seq_per_img]
        cap_lens = torch.Tensor(cap_lens.tolist()).to(torch.int64)
        imgs, caps, cap_lens = fc_feats.to(device), labels.to(device).to(torch.int64), cap_lens.to(device)

        matching_caps = caps.clone()

        # select only 1 out of 5 captions for each image
        caps = caps[:, 0, :]

        # cap_lens = cap_lens.squeeze(-1)

        if not args.use_image_features:
            imgs = encoder(imgs)
        preds, caps, output_lens, indices = generator(imgs, caps, cap_lens)
        loss = 0.0
        for i in range(caps.shape[0]):
            loss += criterion(preds[i, :], caps[i, 1:])
        loss = loss / (1.0 * caps.shape[0])
        # loss += args.alpha_c * ((1. - alphas.sum(dim=1)) ** 2).mean()
        preds_clone = preds.clone()
        preds = pack_padded_sequence(preds, output_lens, batch_first=True)[0]
        targets = pack_padded_sequence(caps[:, 1:], output_lens, batch_first=True)[0]
        top1_acc = categorical_accuracy(preds, targets, 1)
        top1.update(top1_acc, sum(output_lens))
        top5_acc = categorical_accuracy(preds, targets, 5)
        top5.update(top5_acc, sum(output_lens))
        losses.update(loss.item(), sum(output_lens))

        matching_caps = matching_caps[indices]
        for cap_set in matching_caps.tolist():
            refs = []
            for caption in cap_set:
                cap = [word_id for word_id in caption
                       if word_id != word_index['<start>'] and word_id != word_index['<pad>']]
                refs.append(cap)
            references.append(refs)

        fake_caps, _ = generator.sample(cap_len=max(max(cap_lens), args.max_len),
                                        col_shape=caps.shape[1], img_feats=imgs[indices],
                                        input_word=caps[:, 0], sampling_method='max')
        word_idxs, _ = pad_generated_captions(fake_caps.cpu().numpy(), word_index)
        for idxs in word_idxs.tolist():
            hypotheses.append([idx for idx in idxs if idx != word_index['<start>'] and idx != word_index['<pad>']])

        word_idxs = torch.max(preds_clone, dim=2)[1]
        word_idxs, _ = pad_generated_captions(word_idxs.cpu().numpy(), word_index)
        for idxs in word_idxs.tolist():
            hypotheses_tf.append(
                [idx for idx in idxs if idx != word_index['<start>'] and idx != word_index['<pad>']])

        bleu_1 = corpus_bleu(references, hypotheses, weights=(1, 0, 0, 0))
        bleu_2 = corpus_bleu(references, hypotheses, weights=(0.5, 0.5, 0, 0))
        bleu_3 = corpus_bleu(references, hypotheses, weights=(0.33, 0.33, 0.33, 0))
        bleu_4 = corpus_bleu(references, hypotheses)

        bleu_1_tf = corpus_bleu(references, hypotheses_tf, weights=(1, 0, 0, 0))
        bleu_2_tf = corpus_bleu(references, hypotheses_tf, weights=(0.5, 0.5, 0, 0))
        bleu_3_tf = corpus_bleu(references, hypotheses_tf, weights=(0.33, 0.33, 0.33, 0))
        bleu_4_tf = corpus_bleu(references, hypotheses_tf)

        logging.info('VALIDATION\n')
        logging.info('Epoch: [{}]\t'
                     'Loss [{:.4f}]\t'
                     'Top 5 accuracy [{:.4f}]\t'
                     'Top 1 accuracy [{:.4f}]\n'
                     'bleu-1 [{:.3f}]\t'
                     'bleu-2 [{:.3f}]\t'
                     'bleu-3 [{:.3f}]\t'
                     'bleu-4 [{:.3f}]\n'
                     'TF bleu-1 [{:.3f}]\t'
                     'TF bleu-2 [{:.3f}]\t'
                     'TF bleu-3 [{:.3f}]\t'
                     'TF bleu-4 [{:.3f}]\t'.format(epoch, losses.avg, top5.avg, top1.avg, bleu_1, bleu_2,
                                                   bleu_3, bleu_4, bleu_1_tf, bleu_2_tf, bleu_3_tf, bleu_4_tf))

        if args.save_stats:
            with open(args.storage + '/stats/' + args.dataset + '/gen/VAL_MLE_GEN.csv', 'a+') as file:
                writer = csv.writer(file)
                writer.writerow([epoch, losses.avg, losses.val, top5.avg, top5.val, top1.avg, top1.val,
                                 bleu_1, bleu_2, bleu_3, bleu_4, bleu_1_tf, bleu_2_tf, bleu_3_tf, bleu_4_tf])


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Maximum Likelihood Estimation Training')
    parser.add_argument('--batch-size', type=int, default=64)
    parser.add_argument('--epochs', type=int, default=200)
    parser.add_argument('--lr', type=float, default=5e-4)
    parser.add_argument('--clip', type=float, default=10.0)
    parser.add_argument('--alpha-c', type=float, default=1.)
    parser.add_argument('--step-size', type=float, default=5)
    parser.add_argument('--print-freq', type=int, default=50)
    parser.add_argument('--cnn-architecture', type=str, default='resnet152')
    parser.add_argument('--storage', type=str, default='.')
    parser.add_argument('--image-path', type=str, default='images')
    parser.add_argument('--dataset', type=str, default='coco')
    parser.add_argument('--embedding-dim', type=int, default=512)
    parser.add_argument('--checkpoint-filename', type=str, default='')
    parser.add_argument('--use-image-features', type=bool, default=True)
    parser.add_argument('--attention-dim', type=int, default=512)
    parser.add_argument('--gru-units', type=int, default=512)
    parser.add_argument('--max-len', type=int, default=17)
    parser.add_argument('--save-model', type=bool, default=True)
    parser.add_argument('--save-stats', type=bool, default=True)
    parser.add_argument('--workers', type=int, default=2)

    main(parser.parse_args())